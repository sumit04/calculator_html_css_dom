let runningTotal = 0;
let buffer = '0';
let previousSymbol = null;

const screen = document.querySelector('.screen');

function buttonclicked(value) {
  //   console.log(value);

  if (isNaN(value)) {
    handelSymbol(value);
  } else {
    handelNumber(value);
  }
  screen.innerText = buffer;
}

function handelNumber(numberString) {
  if (buffer === '0') {
    buffer = numberString;
  } else {
    buffer += numberString;
  }
}

function handelSymbol(symbol) {
  switch (symbol) {
    case 'c': {
      buffer = '0';
      runningTotal = 0;
      previousSymbol = null;
      break;
    }
    case '←': {
      if (buffer.length === 1) {
        buffer = '0';
      }
      buffer = buffer.substring(0, buffer.length - 1);
      break;
    }
    case '=': {
      if (previousSymbol === null) {
        return;
      }

      flushOperation(parseInt(buffer));
      previousSymbol = null;
      buffer = runningTotal.toString();
      runningTotal = 0;
      break;
    }
    case '÷':
    case '−':
    case '+':
    case '×':
      handelMath(symbol);
      break;
  }
}

function handelMath(symbol) {
  if (buffer === '0') {
    return;
  }
  const intBuffer = parseInt(buffer);

  if (runningTotal === 0) {
    runningTotal = intBuffer;
  } else {
    flushOperation(intBuffer);
  }
  buffer = '0';
  previousSymbol = symbol;
}

function flushOperation(intBuffer) {
  switch (previousSymbol) {
    case '÷': {
      runningTotal /= intBuffer;
      break;
    }
    case '−': {
      runningTotal -= intBuffer;
      break;
    }
    case '+': {
      runningTotal += intBuffer;
      break;
    }
    case '×': {
      runningTotal *= intBuffer;

      break;
    }
  }
  //   buffer = runningTotal;
}
function init() {
  document
    .querySelector('.calc-buttons')
    .addEventListener('click', function(event) {
      buttonclicked(event.target.innerText);
    });
}

init();
